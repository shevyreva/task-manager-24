package ru.t1.shevyreva.tm.exception.field;

public final class IdEmptyException extends AbsrtactFieldException {

    public IdEmptyException() {
        super("Error! Id is empty!");
    }

}
