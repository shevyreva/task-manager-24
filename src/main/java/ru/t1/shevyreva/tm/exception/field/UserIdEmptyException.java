package ru.t1.shevyreva.tm.exception.field;

public final class UserIdEmptyException extends AbsrtactFieldException {

    public UserIdEmptyException() {
        super("Error! User id is empty!");
    }

}
