package ru.t1.shevyreva.tm.exception.user;

public class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Permission is incorrect.");
    }

}
