package ru.t1.shevyreva.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class ApplicationConst {

    @NotNull
    public static final int APP_MAJOR_VERSION = 1;

    @NotNull
    public static final int APP_MINOR_VERSION = 14;

    @NotNull
    public static final int APP_FIXES_VERSION = 0;

}
