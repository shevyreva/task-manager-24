package ru.t1.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.service.IAuthService;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.exception.field.LoginEmptyException;
import ru.t1.shevyreva.tm.exception.field.PasswordEmptyException;
import ru.t1.shevyreva.tm.exception.user.AccessDeniedException;
import ru.t1.shevyreva.tm.exception.user.IncorrectDataException;
import ru.t1.shevyreva.tm.exception.user.NeedLogoutException;
import ru.t1.shevyreva.tm.exception.user.PermissionException;
import ru.t1.shevyreva.tm.model.User;
import ru.t1.shevyreva.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    public void login(
            @Nullable final String login,
            @Nullable final String password) {
        if (isAuth()) throw new NeedLogoutException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = userService.findByLogin(login);
        if (user.getLocked()) throw new IncorrectDataException();
        @Nullable final String hash = HashUtil.salt(password);
        if (hash == null) throw new IncorrectDataException();
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectDataException();
        userId = user.getId();
    }

    public void logout() {
        userId = null;
    }

    private boolean isAuth() {
        return userId != null;
    }

    @NotNull
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        @NotNull final User user = userService.findOneById(userId);
        return user;
    }

    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role userRole = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(userRole);
        if (!hasRole) throw new PermissionException();
    }

}
