package ru.t1.shevyreva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Update status project by Id.";

    @NotNull
    private final String NAME = "project-update-status-by-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, status);
    }
}
