package ru.t1.shevyreva.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String DESCRIPTION = "Show about program.";

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Shevyreva Liya");
        System.out.println("e-mail: liyavmax@gmail.com");
    }

}
