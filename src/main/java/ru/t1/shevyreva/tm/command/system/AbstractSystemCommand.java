package ru.t1.shevyreva.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.service.ICommandService;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @Nullable
    protected ICommandService getCommandService() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getCommandService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
