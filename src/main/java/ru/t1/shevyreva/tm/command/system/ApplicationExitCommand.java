package ru.t1.shevyreva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ApplicationExitCommand extends AbstractSystemCommand {

    @NotNull
    private final String DESCRIPTION = "Close application.";

    @NotNull
    private final String NAME = "exit";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
