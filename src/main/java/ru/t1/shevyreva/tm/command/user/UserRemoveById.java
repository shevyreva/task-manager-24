package ru.t1.shevyreva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserRemoveById extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "User remove by Id.";

    @NotNull
    private final String NAME = "user-remove-by-id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY ID]:");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        getUserService().removeOneById(id);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
