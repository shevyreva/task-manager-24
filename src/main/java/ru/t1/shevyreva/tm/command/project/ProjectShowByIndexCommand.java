package ru.t1.shevyreva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-show-by-index";

    @NotNull
    private final String DESCRIPTION = "Show project by Index.";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW BY INDEX]");
        System.out.println("Enter Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final String userId = getUserId();
        @NotNull final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

}
