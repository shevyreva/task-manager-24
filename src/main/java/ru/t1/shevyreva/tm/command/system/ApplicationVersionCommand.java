package ru.t1.shevyreva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.constant.ApplicationConst;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private final String DESCRIPTION = "Show program version.";

    @NotNull
    private final String NAME = "version";

    @NotNull
    private final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.printf("%s.%s.%s \n", ApplicationConst.APP_MAJOR_VERSION, ApplicationConst.APP_MINOR_VERSION, ApplicationConst.APP_FIXES_VERSION);
    }

}
