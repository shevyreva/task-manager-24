package ru.t1.shevyreva.tm.model;


import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
@Setter
public abstract class AbstractModel {

    @NotNull
    protected String id = UUID.randomUUID().toString();

}
