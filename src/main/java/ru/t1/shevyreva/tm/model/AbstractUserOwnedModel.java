package ru.t1.shevyreva.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    protected String userId;

}
