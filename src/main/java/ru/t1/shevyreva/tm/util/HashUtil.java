package ru.t1.shevyreva.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface HashUtil {

    @NotNull String SECRET = "34534";

    @NotNull Integer ITERATION = 7657;

    @Nullable
    static String salt(@Nullable final String value) {
        if (value == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    @Nullable
    static String md5(@Nullable final String value) {
        try {
            if (value == null) return null;
            @NotNull java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull byte[] array = md.digest(value.getBytes());
            @NotNull StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (@NotNull java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
