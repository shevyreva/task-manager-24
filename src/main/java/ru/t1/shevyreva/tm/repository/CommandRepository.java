package ru.t1.shevyreva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.ICommandRepository;
import ru.t1.shevyreva.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByCommand = new TreeMap<>();

    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return mapByCommand.get(name);
    }

    @NotNull
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        return mapByArgument.get(argument);
    }

    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String name = command.getName();
        if (name != null && !name.isEmpty()) mapByCommand.put(name, command);
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @NotNull
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByCommand.values();
    }

}
