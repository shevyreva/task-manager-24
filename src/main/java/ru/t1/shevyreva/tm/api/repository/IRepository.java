package ru.t1.shevyreva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull final M model);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    void removeAll();

    @Nullable
    M findOneById(@NotNull String Id);

    @NotNull
    M findOneByIndex(@NotNull Integer index);

    @NotNull
    M removeOne(@NotNull M model);

    @NotNull
    M removeOneById(@NotNull String Id);

    void removeOneByIndex(@NotNull Integer index);

    boolean existsById(@NotNull String id);

    @NotNull
    Integer getSize();

}
