package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(M model);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    void removeAll();

    @NotNull
    M findOneById(@Nullable String Id);

    @NotNull
    M findOneByIndex(@Nullable Integer index);

    @NotNull
    M removeOne(@Nullable M model);

    @NotNull
    M removeOneById(@Nullable String Id);

    void removeOneByIndex(@Nullable Integer index);

    boolean existsById(@Nullable String id);

    @NotNull
    Integer getSize();

}
