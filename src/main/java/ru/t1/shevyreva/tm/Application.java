package ru.t1.shevyreva.tm;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.component.Bootstrap;

public final class Application {

    public static void main(@Nullable String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
